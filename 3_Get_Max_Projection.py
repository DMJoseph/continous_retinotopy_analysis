from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys
import imageio
import numpy as np
from scipy import ndimage
import skimage.transform
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib import cm
import PIL.Image
from skimage.transform import resize
from scipy import stats
import h5py
import os


def check_led_colours(blue_file, violet_file):

    blue_data_container = h5py.File(blue_file, 'r')
    blue_array = blue_data_container["Data"]

    violet_data_container = h5py.File(violet_file, 'r')
    violet_array = violet_data_container["Data"]

    print("Blue File: ", blue_file)

    figure_1 = plt.figure()
    axes_1 = figure_1.subplots(1, 2)

    blue_image = blue_array[:, 0]
    blue_image = np.reshape(blue_image, (600,608))
    axes_1[0].set_title("Blue?")
    axes_1[0].imshow(blue_image)

    violet_image = violet_array[:, 0]
    violet_image = np.reshape(violet_image, (600,608))
    axes_1[1].set_title("Violet?")
    axes_1[1].imshow(violet_image)

    plt.ion()
    plt.draw()
    plt.pause(1)

    correct = input("correct?")  # Python 2

    if correct == "n":

        blue_file_intermediate   = blue_file + "t"
        violet_file_intermediate = violet_file + "t"
        print(blue_file_intermediate)

        os.rename(blue_file, blue_file_intermediate)
        os.rename(violet_file, violet_file_intermediate)

        os.rename(violet_file_intermediate, blue_file)
        os.rename(blue_file_intermediate, violet_file)

    plt.close()
    plt.ioff()

def get_max_projection(file, home_directory):

    data_container = h5py.File(file, 'r')
    array = data_container["Data"]
    print("Getting Max Projection")

    sample = array[:, 0:1000]
    max_projection = np.max(sample, axis=1)
    max_projection = np.reshape(max_projection, (600, 608))

    plt.imshow(max_projection)
    plt.show()

    np.save(home_directory + "/max_projection", max_projection)


def get_blue_file(base_directory):
    file_list = os.listdir(base_directory)
    for file in file_list:
        if "Blue" in file:
            return base_directory + "/" + file


def get_violet_file(base_directory):
    file_list = os.listdir(base_directory)
    for file in file_list:
        if "Violet" in file:
            return base_directory + "/" + file





if __name__ == '__main__':

    # Load Data
    full_path = r"/mnt/usb-Realtek_RTL9210B_NVME_012345678995-0:0-part2/Continous_Retinotopy_Attempt_2"

    # Load Data
    blue_file = get_blue_file(full_path)
    violet_file = get_violet_file(full_path)

     # Check LED Colours
    check_led_colours(blue_file, violet_file)

    # Get Max Projection
    get_max_projection(blue_file, full_path)


